"""petite calculette sympatique"""

class SimpleCalculator:     #on appelle SimpleCalculator le nom de la classe

    """on défini la classe SimpleCalculator"""

    def __init__(self, nombre_a, nombre_b):     #init de la calculatrice avec 2 nombres
        """on place les deux arguments dans le self"""
        self.nombre_a = nombre_a
        self.nombre_b = nombre_b

    def addition(self):
        """donne la somme"""
        return self.nombre_a + self.nombre_b

    def soustraction(self):
        """donne la soustraction"""
        return self.nombre_a - self.nombre_b

    def produit(self):
        """donne le produit"""
        return self.nombre_a * self.nombre_b

    def quotient(self):
        """donne le quotient"""
        if self.nombre_b != 0:
            return self.nombre_a / self.nombre_b
        return "pas de division par 0"


TEST = SimpleCalculator(27, 36)
print(TEST.addition())
print(TEST.soustraction())
print(TEST.produit())
print(TEST.quotient())
